# Jhipster

### Common

* [**jhipster-registry**](https://gitlab.com/jhipster-demo/jhipster-registry) - Service Registry for JHipster.

* [**jhipster-jdl**](https://gitlab.com/jhipster-demo/jhipster-jdl) - JDL templates used to generate entities and application code for the projects

### Basic Demo

Projects:
   * [**jhipster-store**](https://gitlab.com/jhipster-demo/jhipster-store) - ReactJS JHipster Gateway. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-store/blob/master/.yo-rc.json) for more application details. The JDL template [``ecommerce-storegw.jh``](https://gitlab.com/jhipster-demo/jhipster-jdl/blob/master/ecommerce-storegw.jh) is used to generate application code for Product, Invoice and Notification and checked into [``importjdl``](https://gitlab.com/jhipster-demo/jhipster-store/tree/importjdl) branch.

   * [**jhipster-product**](https://gitlab.com/jhipster-demo/jhipster-product) - JHipster Microservice for Product. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-product/blob/master/.yo-rc.json) for more application details. The JDL template [``ecommerce-productms.jh``](https://gitlab.com/jhipster-demo/jhipster-jdl/blob/master/ecommerce-productms.jh) is used to generate application code for Product and checked into [``importjdl``](https://gitlab.com/jhipster-demo/jhipster-product/tree/importjdl) branch.

   * [**jhipster-invoice**](https://gitlab.com/jhipster-demo/jhipster-invoice) - JHipster Microservice for Invoice. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-invoice/blob/master/.yo-rc.json) for more application details. The JDL template [``ecommerce-invoicems.jh``](https://gitlab.com/jhipster-demo/jhipster-jdl/blob/master/ecommerce-invoicems.jh) is used to generate application code for Invoice and checked into [``importjdl``](https://gitlab.com/jhipster-demo/jhipster-invoice/tree/importjdl) branch.

   * [**jhipster-notification**](https://gitlab.com/jhipster-demo/jhipster-notification) - JHipster Microservices for Notification. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-notification/blob/master/.yo-rc.json) for more application details. The JDL template [``ecommerce-notificationms.jh``](https://gitlab.com/jhipster-demo/jhipster-jdl/blob/master/ecommerce-notificationms.jh) is used to generate application code for Notification and checked into [``importjdl``](https://gitlab.com/jhipster-demo/jhipster-notification/tree/importjdl) branch.

### UAA projects

Projects:
   * [**jhipster-uaa**](https://gitlab.com/jhipster-demo/jhipster-uaa) - JHipster UAA. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-uaa/blob/master/.yo-rc.json) for more application details.

   * [**jhipster-store-uaa**](https://gitlab.com/jhipster-demo/jhipster-store-uaa) - ReactJS JHipster Gateway using JHipster UAA. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-store-uaa/blob/master/.yo-rc.json) for more application details.

   * [**jhipster-product-uaa**](https://gitlab.com/jhipster-demo/jhipster-product-uaa) - JHipster Microservice for Product using JHipster UAA. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-product-uaa/blob/master/.yo-rc.json) for more application details.

   * [**jhipster-invoice-uaa**](https://gitlab.com/jhipster-demo/jhipster-invoice-uaa) - JHipster Microservice for Invoice using JHipster UAA. Refer to [yoman](https://gitlab.com/jhipster-demo/jhipster-invoice-uaa/blob/master/.yo-rc.json) for more application details.


